package tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.Patient;

public class DagligFastTest {
	private Patient p1;
	private Laegemiddel lm1;
	private LocalDate startDen;
	private LocalDate slutDen;

	@Before
	public void setup() {
		p1 = new Patient("123456-7890", "Dorthe Jensen", 75.5);
		lm1 = new Laegemiddel("Acetylsalicylsyre", 1, 2, 0, "styk");
		startDen = LocalDate.of(2021, 3, 8);
		slutDen = startDen;
	}

	// Døgndosis
	@Test
	public void TC1DoegnDosis0Doser() {
		Ordination o1 = new DagligFast(startDen, slutDen, p1, lm1, 0, 0, 0, 0);
		assertEquals(0, o1.doegnDosis(), 0);
	}

	@Test
	public void TC2DoegnDosis4Doser() {
		Ordination o1 = new DagligFast(startDen, slutDen, p1, lm1, 1, 1, 1, 1);
		assertEquals(4, o1.doegnDosis(), 0);
	}

	// SamletDosis
	@Test
	public void TC3SamletDosis1Dag() {
		Ordination o1 = new DagligFast(startDen, slutDen, p1, lm1, 1, 1, 1, 1);
		assertEquals(4, o1.samletDosis(), 0);
	}

	@Test
	public void TC4SamletDosis1Måned() {
		Ordination o1 = new DagligFast(startDen, LocalDate.of(2021, 4, 7), p1, lm1, 1, 1, 1, 1);
		assertEquals(124, o1.samletDosis(), 0);
	}

	@Test
	public void TC5SamletDosis1År() {
		Ordination o1 = new DagligFast(startDen, LocalDate.of(2022, 3, 7), p1, lm1, 1, 1, 1, 1);
		assertEquals(1460, o1.samletDosis(), 0);
	}

	// Constructor
	@Test
	public void TC6DagligFastNormal() {
		DagligFast o1 = new DagligFast(startDen, slutDen, p1, lm1, 1, 1, 1, 1);
		assertEquals(o1, p1.getOrdinationer().get(0));
		assertEquals(lm1, o1.getLaegemiddel());
		Dosis[] doser = o1.getDoser();
		LocalTime[] dates = { LocalTime.of(6, 0), LocalTime.of(12, 0), LocalTime.of(18, 0), LocalTime.of(23, 59) };
		for (int i = 0; i < doser.length; i++) {
			Dosis d = doser[i];
			LocalTime t = dates[i];
			assertEquals(1, d.getAntal(), 0);
			assertEquals(t, d.getTid());
		}
	}

	@Test
	public void TC7DagligFastIngenLM() {
		DagligFast o1 = new DagligFast(startDen, LocalDate.of(2021, 3, 8), p1, null, 1, 1, 1, 1);
		assertEquals(o1, p1.getOrdinationer().get(0));
		assertEquals(null, o1.getLaegemiddel());
		Dosis[] doser = o1.getDoser();
		LocalTime[] dates = { LocalTime.of(6, 0), LocalTime.of(12, 0), LocalTime.of(18, 0), LocalTime.of(23, 59) };
		for (int i = 0; i < doser.length; i++) {
			Dosis d = doser[i];
			LocalTime t = dates[i];
			assertEquals(1, d.getAntal(), 0);
			assertEquals(t, d.getTid());
		}
	}

}
