package tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	Patient patient;
	Laegemiddel laegemiddel;
	LocalDate dato;
	Controller controller;

	@Before
	public void setup() {
		
		controller = Controller.getTestController();
		
		patient = controller.opretPatient("123456-7890", "Dorthe Jensen", 75.5);
		laegemiddel = controller.opretLaegemiddel("Acetylsalicylsyre", 0.05, 0.10, 0.15, "Styk");
		dato = LocalDate.of(2021, 3, 8);
	}

	@Test
	public void TC1testopretPNOrdination() {
		PN pn1 = controller.opretPNOrdination(dato, dato, patient, laegemiddel, 1);
		assertEquals(laegemiddel, pn1.getLaegemiddel());
		assertEquals(pn1, patient.getOrdinationer().get(0));
		assertEquals(1, pn1.getAntalEnheder(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TC2testopretPNOrdination() {
		controller.opretPNOrdination(dato, LocalDate.of(2021, 3, 7), patient, laegemiddel, 1);
	}

	@Test
	public void TC3testopretPNOrdination() {
		PN pn1 = controller.opretPNOrdination(dato, dato, patient, null, 1);
		assertEquals(null, pn1.getLaegemiddel());
		assertEquals(pn1, patient.getOrdinationer().get(0));
		assertEquals(1, pn1.getAntalEnheder(), 0);
	}

	@Test
	public void TC1testopretDagligFastOrdination() {
		DagligFast d1 = controller.opretDagligFastOrdination(dato, dato, patient, laegemiddel, 1, 1, 1, 1);

		assertEquals(d1, patient.getOrdinationer().get(0));
		assertEquals(laegemiddel, d1.getLaegemiddel());
		Dosis[] doser = d1.getDoser();
		LocalTime[] dates = { LocalTime.of(6, 0), LocalTime.of(12, 0), LocalTime.of(18, 0), LocalTime.of(23, 59) };
		for (int i = 0; i < doser.length; i++) {
			Dosis d = doser[i];
			LocalTime t = dates[i];
			assertEquals(1, d.getAntal(), 0);
			assertEquals(t, d.getTid());
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void TC2testopretDagligFastOrdination() {
		controller.opretDagligFastOrdination(dato, LocalDate.of(2021, 3, 7), patient, laegemiddel, 1, 1, 1, 1);
	}

	@Test
	public void TC3opretDagligFastOrdination() {
		DagligFast d1 = controller.opretDagligFastOrdination(dato, dato, patient, null, 1, 1, 1, 1);
		assertEquals(d1, patient.getOrdinationer().get(0));
		assertEquals(null, d1.getLaegemiddel());
		Dosis[] doser = d1.getDoser();
		LocalTime[] dates = { LocalTime.of(6, 0), LocalTime.of(12, 0), LocalTime.of(18, 0), LocalTime.of(23, 59) };
		for (int i = 0; i < doser.length; i++) {
			Dosis d = doser[i];
			LocalTime t = dates[i];
			assertEquals(1, d.getAntal(), 0);
			assertEquals(t, d.getTid());
		}
	}

	@Test
	public void TC1testdagligSkaev() {
		LocalTime[] dates = { LocalTime.of(8, 0), LocalTime.of(12, 0), LocalTime.of(18, 0), LocalTime.of(20, 00) };
		double[] antalEnheder = { 1, 2, 1, 3 };

		DagligSkaev dagligskaev = controller.opretDagligSkaevOrdination(dato, dato, patient, laegemiddel, dates,
				antalEnheder);

		assertEquals(laegemiddel, dagligskaev.getLaegemiddel());
		assertEquals(dagligskaev, patient.getOrdinationer().get(0));
		ArrayList<Dosis> doser = dagligskaev.getDoser();
		for (int i = 0; i < doser.size(); i++) {
			Dosis d = doser.get(i);
			LocalTime t = dates[i];
			assertEquals(antalEnheder[i], d.getAntal(), 0);
			assertEquals(t, d.getTid());
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void TC2testopretDagligSkaev() {
		LocalTime[] dates = { LocalTime.of(8, 0), LocalTime.of(12, 0), LocalTime.of(18, 0), LocalTime.of(20, 00) };
		double[] antalEnheder = { 1, 2, 1, 3 };
		controller.opretDagligSkaevOrdination(dato, LocalDate.of(2021, 3, 7), patient, laegemiddel, dates,
				antalEnheder);
	}

	@Test
	public void TC3testdagligSkaev() {
		LocalTime[] dates = { LocalTime.of(8, 0), LocalTime.of(12, 0), LocalTime.of(18, 0), LocalTime.of(20, 00) };
		double[] antalEnheder = { 1, 1, 1, 1 };

		DagligSkaev dagligskaev = controller.opretDagligSkaevOrdination(dato, dato, patient, null, dates, antalEnheder);

		assertEquals(null, dagligskaev.getLaegemiddel());
		assertEquals(dagligskaev, patient.getOrdinationer().get(0));
		ArrayList<Dosis> doser = dagligskaev.getDoser();
		for (int i = 0; i < doser.size(); i++) {
			Dosis d = doser.get(i);
			LocalTime t = dates[i];
			assertEquals(1, d.getAntal(), 0);
			assertEquals(t, d.getTid());
		}
	}

	@Test
	public void TC1testordinationPNAnvendt() {
		PN pn1 = controller.opretPNOrdination(dato, dato, patient, laegemiddel, 1);
		controller.ordinationPNAnvendt(pn1, dato);
		assertEquals(1, pn1.getAntalGangeGivet());
	}

	@Test(expected = IllegalArgumentException.class)
	public void TC2testordinationPNAnvendt() {
		PN pn1 = controller.opretPNOrdination(dato, dato, patient, laegemiddel, 1);
		controller.ordinationPNAnvendt(pn1, LocalDate.of(2021, 3, 7));
	}

	@Test
	public void TC1testanbefaletDosisPrDoegn() {
		Patient p1 = new Patient("123456-7890", "Dorthe Jensen", 24);
		double anbefalet = controller.anbefaletDosisPrDoegn(p1, laegemiddel);

		assertEquals(1.2, anbefalet, 0.0001);
	}

	@Test
	public void TC2testanbefaletDosisPrDoegn() {
		Patient p1 = new Patient("123456-7890", "Dorthe Jensen", 75.5);
		double anbefalet = controller.anbefaletDosisPrDoegn(p1, laegemiddel);

		assertEquals(7.55, anbefalet, 0.0001);
	}

	@Test
	public void TC3testanbefaletDosisPrDoegn() {
		Patient p1 = new Patient("123456-7890", "Dorthe Jensen", 150);
		double anbefalet = controller.anbefaletDosisPrDoegn(p1, laegemiddel);

		assertEquals(22.5, anbefalet, 0.0001);
	}

	@Test
	public void TC1testantalOrdinationerPrVægtprLægemiddel() {
		Laegemiddel para = controller.opretLaegemiddel("Paracetamol", 0.05, 0.10, 0.15, "Styk");
		Patient p1 = controller.opretPatient("123456-7890", "Dorthe Jensen", 75.5);
		Patient p2 = controller.opretPatient("123456-7890", "Jens Pedersen", 80.5);
		new DagligFast(dato, dato, p1, laegemiddel, 1, 1, 1, 1);
		new DagligFast(dato, dato, p2, laegemiddel, 1, 1, 1, 1);

		int antalordinationer = controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, para);
		assertEquals(0, antalordinationer, 0);
	}

	@Test
	public void TC2testantalOrdinationerPrVægtprLægemiddel() {
		Patient p1 = controller.opretPatient("123456-7890", "Dorthe Jensen", 75.5);
		Patient p2 = controller.opretPatient("123456-7890", "Jens Pedersen", 80.5);
		new DagligFast(dato, dato, p1, laegemiddel, 1, 1, 1, 1);
		new DagligFast(dato, dato, p2, laegemiddel, 1, 1, 1, 1);
		
		int antalordinationer = controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, laegemiddel);
		assertEquals(2, antalordinationer, 0);
	}

}
