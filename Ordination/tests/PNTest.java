 package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PNTest {
	private Patient p1;
	private Laegemiddel lm1;
	private LocalDate startDen;
	private LocalDate slutDen;

	@Before
	public void setup() {
		p1 = new Patient("123456-7890", "Dorthe Jensen", 75.5);
		lm1 = new Laegemiddel("Acetylsalicylsyre", 1, 2, 0, "styk");
		startDen = LocalDate.of(2021, 3, 8);
		slutDen = LocalDate.of(2021, 3, 20);
	}
	
	//Døgndosis
	@Test
	public void TC1DøgnDosisFlereGivningerSammeDag() {
		PN pn1 = new PN(startDen, slutDen, p1, lm1, 1);
		for (int i = 0; i < 5; i++) {
			pn1.givDosis(startDen);
		}
		assertEquals(5, pn1.doegnDosis(),0);
	}
	
	@Test
	public void TC2DøgnDosisFlereGivningerFlereDage() {
		System.out.println("STARTING TC2");
		PN pn1 = new PN(startDen, slutDen, p1, lm1, 1);
		pn1.givDosis(LocalDate.of(2021, 3, 8));
		pn1.givDosis(LocalDate.of(2021, 3, 9));
		pn1.givDosis(LocalDate.of(2021, 3, 11));
		assertEquals(3.0/4.0, pn1.doegnDosis(),0.01);
	}
	
	@Test
	public void TC3DøgnDosisIngenGivninger() {
		PN pn1 = new PN(startDen, slutDen, p1, lm1, 1);
		
		assertEquals(0, pn1.doegnDosis(),0.01);
	}
	
	//Samlet Dosis
	@Test
	public void TC4SamletDosisFlereGivningerSammeDag() {
		PN pn1 = new PN(startDen, slutDen, p1, lm1, 1);
		for (int i = 0; i < 5; i++) {
			pn1.givDosis(startDen);
		}
		
		assertEquals(5, pn1.samletDosis(),0);
	}
	
	@Test
	public void TC5SamletDosisFlereGivningerFlereDage() {
		PN pn1 = new PN(startDen, slutDen, p1, lm1, 1);
		pn1.givDosis(LocalDate.of(2021, 3, 8));
		pn1.givDosis(LocalDate.of(2021, 3, 9));
		pn1.givDosis(LocalDate.of(2021, 3, 11));
		assertEquals(3, pn1.samletDosis(),0.01);
	}
	
	@Test
	public void TC6SamletDosisIngenGivninger() {
		PN pn1 = new PN(startDen, slutDen, p1, lm1, 1);
		
		assertEquals(0, pn1.samletDosis(),0.01);
	}
	
	//Constructor
	@Test
	public void TC7PNNormal() {
		PN pn1 = new PN(startDen, slutDen, p1, lm1, 1);
		assertEquals(lm1, pn1.getLaegemiddel());
		assertEquals(pn1, p1.getOrdinationer().get(0));
		assertEquals(1, pn1.getAntalEnheder(),0);
	}
	
	@Test
	public void TC8PNIngenLægemiddel() {
		PN pn1 = new PN(startDen, slutDen, p1, null, 1);
		assertEquals(null, pn1.getLaegemiddel());
		assertEquals(pn1, p1.getOrdinationer().get(0));
		assertEquals(1, pn1.getAntalEnheder(),0);
	}
	
	//Givdosis
	@Test
	public void TC8GivDosisKorrekt() {
		
		PN pn1 = new PN(startDen, slutDen, p1, lm1, 1);
		assertEquals(0, pn1.getAntalGangeGivet(),0);
		assertTrue(pn1.givDosis(startDen));
		assertEquals(1, pn1.getAntalGangeGivet(),0);
		
	}
	
	@Test
	public void TC9GivDosisInkorrekt() {
		PN pn1 = new PN(startDen, slutDen, p1, lm1, 1);
		assertFalse(pn1.givDosis(LocalDate.of(2021, 3, 7)));
		assertEquals(0, pn1.getAntalGangeGivet(),0);
	}
	
}
