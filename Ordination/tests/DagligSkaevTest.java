package tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkaevTest {

	Patient patient;
	Laegemiddel laegemiddel;
	LocalDate dato;

	@Before
	public void setup() {
		patient = new Patient("123456-7890", "Dorthe Jensen", 75.5);
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.05, 0.10, 0.15, "Styk");
		dato = LocalDate.of(2021, 3, 8);

	}

	@Test
	public void TC1DoegnDosis0Doser() {
		DagligSkaev dagligskaev = new DagligSkaev(dato, dato, patient, laegemiddel);
		dagligskaev.opretDosis(LocalTime.of(8, 0), 0);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 0);
		dagligskaev.opretDosis(LocalTime.of(16, 0), 0);
		dagligskaev.opretDosis(LocalTime.of(20, 0), 0);

		double a1 = dagligskaev.doegnDosis();
		assertEquals(0, a1, 0.001);
	}

	@Test
	public void TC2DoegnDosis7Doser() {
		DagligSkaev dagligskaev = new DagligSkaev(dato, dato, patient, laegemiddel);
		dagligskaev.opretDosis(LocalTime.of(8, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 2);
		dagligskaev.opretDosis(LocalTime.of(16, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(20, 0), 3);

		double a1 = dagligskaev.doegnDosis();
		assertEquals(7, a1, 0.001);

	}

	@Test
	public void TC3SamletDosis1Dag() {
		DagligSkaev dagligskaev = new DagligSkaev(dato, dato, patient, laegemiddel);
		dagligskaev.opretDosis(LocalTime.of(8, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(16, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(20, 0), 1);

		double a1 = dagligskaev.samletDosis();
		assertEquals(4, a1, 0.001);
	}

	@Test
	public void TC4SamletDosis1Måned() {
		DagligSkaev dagligskaev = new DagligSkaev(dato, LocalDate.of(2021, 4, 7), patient, laegemiddel);
		dagligskaev.opretDosis(LocalTime.of(8, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(16, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(20, 0), 1);

		double a1 = dagligskaev.samletDosis();
		assertEquals(124, a1, 0.001);
	}

	@Test
	public void TC5SamletDosis1År() {
		DagligSkaev dagligskaev = new DagligSkaev(dato, LocalDate.of(2022, 3, 7), patient, laegemiddel);
		dagligskaev.opretDosis(LocalTime.of(8, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(16, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(20, 0), 1);

		double a1 = dagligskaev.samletDosis();
		assertEquals(1460, a1, 0.001);
	}

	@Test
	public void TC6DagligSkævNormal() {
		DagligSkaev dagligskaev = new DagligSkaev(dato, dato, patient, laegemiddel);
		dagligskaev.opretDosis(LocalTime.of(8, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(16, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(20, 0), 1);

		assertEquals(laegemiddel, dagligskaev.getLaegemiddel());
		assertEquals(dagligskaev, patient.getOrdinationer().get(0));
		ArrayList<Dosis> doser = dagligskaev.getDoser();
		LocalTime[] dates = { LocalTime.of(8, 0), LocalTime.of(14, 0), LocalTime.of(16, 0), LocalTime.of(20, 00) };
		for (int i = 0; i < doser.size(); i++) {
			Dosis d = doser.get(i);
			LocalTime t = dates[i];
			assertEquals(1, d.getAntal(), 0);
			assertEquals(t, d.getTid());
		}
	}

	@Test
	public void TC7DagligSkævIngenLM() {
		DagligSkaev dagligskaev = new DagligSkaev(dato, dato, patient, null);
		dagligskaev.opretDosis(LocalTime.of(8, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(14, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(16, 0), 1);
		dagligskaev.opretDosis(LocalTime.of(20, 0), 1);

		assertEquals(null, dagligskaev.getLaegemiddel());
		assertEquals(dagligskaev, patient.getOrdinationer().get(0));
		ArrayList<Dosis> doser = dagligskaev.getDoser();
		LocalTime[] dates = { LocalTime.of(8, 0), LocalTime.of(14, 0), LocalTime.of(16, 0), LocalTime.of(20, 00) };
		for (int i = 0; i < doser.size(); i++) {
			Dosis d = doser.get(i);
			LocalTime t = dates[i];
			assertEquals(1, d.getAntal(), 0);
			assertEquals(t, d.getTid());
		}
	}
}
