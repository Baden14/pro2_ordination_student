package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private static ArrayList<LocalDate> datoer;

	public PN(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, patient, laegemiddel);
		this.antalEnheder = antalEnheder;
		datoer = new ArrayList<>();
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean givesDenIsAfterStart = ChronoUnit.DAYS.between(getStartDen(), givesDen) >= 0;
		boolean givesDenIsBeforeEnd = ChronoUnit.DAYS.between(givesDen, getSlutDen()) >= 0;
		if (givesDenIsAfterStart && givesDenIsBeforeEnd) {
			datoer.add(givesDen);
			return true;
		} else {
			return false;
		}
	}

	public double doegnDosis() {
		boolean listEmpty = datoer.isEmpty();
		if (listEmpty) {
			return 0;
		}
		double daysBetween = ChronoUnit.DAYS.between(datoer.get(0), datoer.get(datoer.size()-1))+1;
		double result = samletDosis() / Math.max(1, daysBetween);
		return result;
	}

	public double samletDosis() {
		return antalEnheder * getAntalGangeGivet();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return datoer.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
