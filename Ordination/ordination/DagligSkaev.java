package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private static ArrayList<Dosis> dosisList;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel) {
		super(startDen, slutDen, patient, laegemiddel);
		dosisList = new ArrayList<>();
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		dosisList.add(d);
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis d : dosisList) {
			sum += d.getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return "Daglig Skæv";
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(dosisList);
	}
}
