package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

public class DagligFast extends Ordination {
	private static Dosis[] dosisList = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, patient, laegemiddel);
		dosisList[0] = opretDosis(LocalTime.of(6, 0), morgenAntal);
		dosisList[1] = opretDosis(LocalTime.of(12, 0), middagAntal);
		dosisList[2] = opretDosis(LocalTime.of(18, 0), aftenAntal);
		dosisList[3] = opretDosis(LocalTime.of(23, 59), natAntal);
	}

	private Dosis opretDosis(LocalTime tid, double antal) {
		if (antal > 0) {
			return new Dosis(tid, antal);
		}
		return null;
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for(Dosis d : dosisList) {
			if (d!=null) {
				sum += d.getAntal();
			}
		}
		return sum;
	}

	public Dosis[] getDoser() {
		return Arrays.copyOf(dosisList, dosisList.length);
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}
}
